import React from "react";

const routes = [
  {
    path: "/register",
    exact: true,
    name: "Register",
    superAdmin: true,
    component: React.lazy(() => import("./Components/auth/register/Register")),
  },
  {
    path: "/users/list",
    exact: true,
    name: "Admin list",
    superAdmin: true,
    component: React.lazy(() =>
      import("./Components/pages/suppAdmin/UsersList")
    ),
  },

  {
    path: "/filial/list",
    exact: true,
    isAdmin: true,
    name: "Filiale ",
    component: React.lazy(() =>
      import("./Components/pages/admin/Filial/List/List")
    ),
  },
  // {
  //   path: "/filial/list",
  //   exact: true,
  //   isAdmin: true,

  //   name: "Liste Filials",
  //   component: React.lazy(() =>
  //     import("./Components/pages/admin/Filial/List/List")
  //   ),
  // },
  // {
  //   path: "/filial/add",
  //   exact: true,
  //   isAdmin: true,
  //   name: "Ajouter Filial",
  //   component: React.lazy(() =>
  //     import("./Components/pages/admin/Filial/add/Layout")
  //   ),
  // },
  {
    path: "/cards",
    exact: true,
    isAdmin: true,
    name: "Cartes",
    component: React.lazy(() =>
      import("./Components/pages/admin/Card/Cards/Cards")
    ),
  },
  // {
  //   path: "/card",
  //   exact: true,
  //   isAdmin: true,
  //   name: "Carte",
  // },
  {
    path: "/product",
    exact: true,
    isAdmin: true,
    name: "Produits",
    component: React.lazy(() =>
      import("./Components/pages/admin/product/Layout")
    ),
  },
  {
    path: "/ingredient",
    exact: true,
    isAdmin: true,
    name: "Ingrédients",
    component: React.lazy(() =>
      import("./Components/pages/admin/ingrediant/Layout")
    ),
  },
  {
    path: "/commandes",
    exact: true,
    isAdmin: true,
    name: "Commandes",
    component: React.lazy(() =>
      import("./Components/pages/admin/Commandes/Commandes")
    ),
  },
  // {
  //   path: "/appweb",
  //   exact: true,
  //   isAdmin: true,
  //   name: "Applications Web",
  //   component: React.lazy(() =>
  //     import("./Components/pages/admin/AppWeb/AppWeb")
  //   ),
  // },
  // {
  //   path: "/appmobile",
  //   exact: true,
  //   isAdmin: true,
  //   name: "Applications Mobile",
  //   component: React.lazy(() =>
  //     import("./Components/pages/admin/AppMobile/AppMobile")
  //   ),
  // },
  // {
  //   path: "/ecransscreenflex",
  //   exact: true,
  //   isAdmin: true,
  //   name: "Ecrans",
  //   component: React.lazy(() =>
  //     import("./Components/pages/admin/Ecrans/Ecrans")
  //   ),
  // },

  {
    path: "/parametres",
    exact: true,
    isAdmin: true,

    name: "Paramétres",
  },
  {
    path: "/parametres/generaux",
    exact: true,
    isAdmin: true,
    name: "Paramètres Généraux",
    component: React.lazy(() =>
      import(
        "./Components/pages/admin/Parametres/ParamsGeneraux/ParamsGeneraux"
      )
    ),
  },
  {
    path: "/parametres/canaux",
    exact: true,
    isAdmin: true,
    name: "Paramètres des Canaux",
    component: React.lazy(() =>
      import("./Components/pages/admin/Parametres/ParamsCanaux/ParamsCanaux")
    ),
  },
  {
    path: "/404",
    exact: true,
    name: "404 not found",
    component: React.lazy(() => import("./Components/auth/404/404.js")),
  },
];

export default routes;
