export default {
  items: [
    {
      name: "Dashboard Admin",
      url: "/",
      /*   icon: 'icon-speedometer', */
    },

    // {
    //   name: "Filiales",
    //   url: "/filial",
    //   icon: "icon-pencil",
    //   children: [
    //     {
    //       name: "  Liste Filiales",
    //       url: "/filial/list",
    //     },
    //     {
    //       name: "Ajouter Filiale",
    //       url: "/filial/add",
    //     },
    //   ],
    // },
    {
      name: "Filiales",
      url: "/filial/list",
      icon: "icon-cursor",
      // children: [],
    },
    {
      name: "Carte",
      url: "/card",
      icon: "icon-cursor",
      children: [],
    },
    {
      name: "Cartes",
      url: "/cards",
      icon: "icon-cursor",
      // children: [],
    },

    {
      name: "Produits",
      url: "/product",
      icon: "icon-cursor",
    },
    {
      name: "Ingrédients",
      url: "/ingredient",
      icon: "icon-cursor",
    },
    {
      name: "Commandes",
      url: "/commandes",
      icon: "icon-cursor",
    },
    // {
    //   name: "Applications Web",
    //   url: "/appweb",
    //   icon: "icon-cursor",
    // },
    // {
    //   name: "Applications Mobiles",
    //   url: "/appmobile",
    //   icon: "icon-cursor",
    // },
    // {
    //   name: "Ecrans ScreenFlex",
    //   url: "/ecransscreenflex",
    //   icon: "icon-cursor",
    // },

    {
      name: "Paramétres",
      url: "/parametres",
      icon: "icon-pencil",
      children: [
        {
          name: "Paramétres Géneraux",
          url: "/parametres/generaux",
        },
        {
          name: "Paramétres des Canaux",
          url: "/parametres/canaux",
        },
      ],
    },
  ],
};
