import React from "react";
export const generateAdminNav = (restaurant, sideBar) => {
  sideBar["items"][2].children = [];
  sideBar &&
    restaurant.map((rest, index) =>
      rest.card.map((card, inx) =>
        sideBar["items"][2].children.push({
          name: card.name,
          url: "/Card/" + card.name,
        })
      )
    );

  return sideBar;
};
export const generateAdminRoutes = (restaurant, routers) => {
  restaurant.map((rest, index) =>
    rest.card.map((card, inx) =>
      routers.push({
        path: "/Card/" + card.name,
        exact: true,
        isAdmin: true,
        name: card.name,
        cardID: card._id,
        component: React.lazy(() =>
          import("../Components/pages/admin/Card/Card.js")
        ),
      })
    )
  );

  return routers;
};
