import React from "react";
import CartesCardView from "./cartesCardView";
import { Container } from "@material-ui/core";
import { Row, Col } from "reactstrap";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";

import AddCard from "../add/AddCard.js";

import { Button } from "reactstrap";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const List = ({ user }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <Container>
        <div id="AddNewCardBtnContainer">
          <Button type="button" onClick={handleOpen} color="danger">
            Ajouter nouvelle carte
          </Button>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <AddCard />
              </div>
            </Fade>
          </Modal>
        </div>

        <Row>
          {user &&
            user.restaurant.map((rest, index) =>
              rest.card.map((card, inx) => (
                <Col xs="12" md="4" className="mb-4" key={inx}>
                  <CartesCardView card={card} restaurant={rest} />
                </Col>
              ))
            )}
        </Row>
      </Container>
    </div>
  );
};
List.propTypes = {};
const StateProps = (state) => ({
  user: state.auth.user,
});
export default connect(StateProps, null)(withRouter(List));
