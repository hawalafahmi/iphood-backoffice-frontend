/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { InputGroup, Form, Row, Label, Col } from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import AddIcon from "@material-ui/icons/Add";

import { TextField, Input, Container } from "@material-ui/core";
import _ from "lodash";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import "../card.css";
import ImgView from "./ImgView";
import { addNewProduct } from "../../../../../providers/actions/product";
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

const AddProduct = ({ errors, categoryID, refresh, addNewProduct }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [Errors, setErrors] = useState({});
  const [ProductObject, setProductObject] = useState({
    name: "",
    description: "",
    price: "",
    image: "",
  });
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
    setProductObject({
      name: "",
      description: "",
      price: "",
      image: "",
    });
  };

  const onChange = (e) => {
    setProductObject({ ...ProductObject, [e.target.name]: e.target.value });
  };
  const onSubmit = () => {
    var bodyFormData = new FormData();
    bodyFormData.set("name", ProductObject.name);
    bodyFormData.set("description", ProductObject.description);
    bodyFormData.set("price", ProductObject.price);
    bodyFormData.append("image", ProductObject.image);

    addNewProduct(categoryID, bodyFormData);
  };
  useEffect(() => {
    if (errors) setErrors(errors);
  }, [errors]);
  useEffect(() => {
    if (refresh) {
      dispatch({ type: "ADD_PRODUCT_REFRESH" });
      setModal(false);
    }
  }, [refresh]);
  return (
    <div className={classes.root}>
      <Button onClick={toggle} color="danger">
        <Row>
          <Col>
            <div className="add-Product-text">Ajouter un nouveau produit</div>
          </Col>
          <Col xs="2">
            <div className="add-Product-icon">
              <AddIcon />
            </div>
          </Col>
        </Row>
      </Button>

      <Form>
        <Modal className="add-Product-modal" isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Ajouter un nouveau produit </ModalHeader>
          <ModalBody>
            <Container>
              <Row>
                <Col xs="6">
                  <InputGroup className="mb-4">
                    <TextField
                      id="outlined-helperText"
                      label="Nom"
                      variant="outlined"
                      name="name"
                      onChange={(e) => onChange(e)}
                      error={!_.isNil(Errors.name)}
                      helperText={Errors.name}
                    />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <TextField
                      style={{ marginLeft: "1px" }}
                      id="outlined-helperText"
                      label="Description"
                      variant="outlined"
                      name="description"
                      onChange={(e) => onChange(e)}
                      error={!_.isNil(Errors.description)}
                      helperText={Errors.description}
                    />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <TextField
                      style={{ marginLeft: "1px" }}
                      id="outlined-helperText"
                      label="Prix"
                      variant="outlined"
                      name="price"
                      onChange={(e) => onChange(e)}
                      error={!_.isNil(Errors.price)}
                      helperText={Errors.price}
                    />
                  </InputGroup>

                  <InputGroup className="mb-4">
                    <Row>
                      {" "}
                      <Label
                        sm="5"
                        size="sm"
                        htmlFor="filePicker"
                        id="upload-image-filial"
                      >
                        Choisir image...
                      </Label>
                    </Row>
                    <Row>
                      {" "}
                      <div
                        style={{
                          color: "red",
                          marginTop: "10px",
                          marginLeft: "27px",
                        }}
                      >
                        {Errors.image}
                      </div>
                    </Row>

                    <Input
                      id="filePicker"
                      style={{ display: "none" }}
                      type={"file"}
                      name="image"
                      onChange={(e) =>
                        setProductObject({
                          ...ProductObject,
                          [e.target.name]: e.target.files[0],
                        })
                      }
                    />
                  </InputGroup>
                </Col>
                <Col>
                  <ImgView ImgObject={ProductObject} />{" "}
                </Col>
              </Row>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button className="cancel-btn" onClick={toggle}>
              Cancel
            </Button>
            <Button className="save-btn" onClick={(e) => onSubmit()}>
              save
            </Button>
          </ModalFooter>
        </Modal>
      </Form>
    </div>
  );
};

AddProduct.prototype = {
  addNewProduct: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  errors: state.errors,
  user: state.auth.user,
  refresh: state.product.refresh,
});
export default connect(StateProps, {
  addNewProduct,
})(withRouter(AddProduct));
