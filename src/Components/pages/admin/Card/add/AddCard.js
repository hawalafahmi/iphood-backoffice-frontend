/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Label,
} from "reactstrap";
import { connect, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import _ from "lodash";
import "../card.css";
import { addNewCard } from "../../../../../providers/actions/card";
import PropTypes from "prop-types";
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

const AddCard = ({ addNewCard, errors, cardAction }) => {
  // const [modal, setModal] = useState(false);
  const [Errors, setErrors] = useState({});

  // const toggle = () => {
  //   dispatch({ type: "CLEAR_ERRORS" });
  //   setModal(!modal);
  // };
  const dispatch = useDispatch();
  const classes = useStyles();
  const [Card, setCard] = useState({ name: "" });
  const onChange = (e) => setCard({ ...Card, [e.target.name]: e.target.value });

  const onSubmit = () => addNewCard(Card);

  useEffect(() => {
    if (errors) setErrors(errors);
  }, [errors]);
  // useEffect(() => {
  //   if (cardAction) {
  //     toggle();
  //     dispatch({ type: "ADD_CARD_CLEAR" });
  //   }
  // }, [cardAction, dispatch]);
  return (
    <div className={classes.root}>
      {/* <Fab
        color="secondary"
        onClick={toggle}
        aria-label="edit"
        style={{ background: "#ff2828" }}
      >
        <AddIcon />
      </Fab> */}

      <div>Ajouter une nouvelle carte</div>
      <div>
        <Container>
          <Row>
            <Col xs="4">
              <Label style={{ marginTop: "17px" }}>Nom du carte </Label>
            </Col>
            <Col xs="6">
              <TextField
                id="standard-basic"
                label="Nom"
                name="name"
                onChange={(e) => onChange(e)}
                error={!_.isNil(Errors.name)}
                helperText={Errors.name}
              />
            </Col>
          </Row>
        </Container>
      </div>
      <div className="add-card-modal-button-action">
        {/* <div className="cancel-button">
          <Button>Annuler</Button>
        </div> */}
        <div className="save-button">
          <Button onClick={(e) => onSubmit()}>Sauvegarder</Button>
        </div>
      </div>

      {/* <div className="add-card-modal">
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Ajouter une nouvelle carte</ModalHeader>
          <ModalBody>
            <Container>
              <Row>
                <Col xs="4">
                  <Label style={{ marginTop: "17px" }}>Nom du carte </Label>
                </Col>
                <Col xs="6">
                  <TextField
                    id="standard-basic"
                    label="Nom"
                    name="name"
                    onChange={(e) => onChange(e)}
                    error={!_.isNil(Errors.name)}
                    helperText={Errors.name}
                  />
                </Col>
              </Row>
            </Container>
          </ModalBody>
          <ModalFooter className="add-card-modal-button-action">
            <div className="cancel-button">
              <Button onClick={toggle}>Annuler</Button>
            </div>
            <div className="save-button">
              <Button onClick={(e) => onSubmit()}>Sauvegarder</Button>
            </div>
          </ModalFooter>
        </Modal>
      </div> */}
    </div>
  );
};
AddCard.prototype = {
  addNewCard: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  errors: state.errors,
  cardAction: state.card.action,
});
export default connect(StateProps, { addNewCard })(withRouter(AddCard));
