/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import AddIcon from "@material-ui/icons/Add";
import MultiSelect from "react-multi-select-component";
import PropTypes from "prop-types";
import "../card.css";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  getAllProduct,
  addExistingProduct,
} from "../../../../../providers/actions/product";

const AddExistingProduct = ({
  getAllProduct,
  addExistingProduct,
  categoryID,
  currentProduct,
  refresh,
  allProducts,
}) => {
  const [modal, setModal] = useState(false);
  const [selected, setSelected] = useState([]);
  const [Options, setOptions] = useState([]);
  // const [listProduct, setlistProduct] = useState()

  const toggle = () => {
    setSelected([]);
    setModal(!modal);
  };
  const onSubmit = () => {
    if (selected.length > 0) {
      let productArray = [];
      selected.map((items) => productArray.push(items.value));
      addExistingProduct(categoryID, productArray);
    }
  };

  useEffect(() => {
    getAllProduct();
  }, [getAllProduct]);
  useEffect(() => {
    Options.length === 0 &&
      allProducts &&
      allProducts.map((prod, inx) =>
        setOptions((prevState) => [
          ...prevState,
          { label: prod.name, value: prod._id },
        ])
      );
  }, [allProducts]);
  useEffect(() => {
    if (refresh) {
      setSelected([]);
      setModal(false);
    }
  }, [refresh]);
  return (
    <div>
      <Button
        onClick={toggle}
        className="add-category"
        id="AddExistingProductBtn"
        color="danger"
      >
        <Row>
          <Col>
            <div className="add-category-text">Ajouter un produit existant</div>
          </Col>
          <Col xs="2">
            <div className="add-category-icon">
              <AddIcon />
            </div>
          </Col>
        </Row>
      </Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Choisir produits</ModalHeader>
        <ModalBody>
          <div>
            <MultiSelect
              options={Options}
              value={selected}
              onChange={setSelected}
              labelledBy={"Selectionner"}
            />
          </div>
        </ModalBody>
        <ModalFooter className="add-card-modal-button-action">
          <div className="cancel-button">
            <Button onClick={toggle}>Annuler</Button>
          </div>
          <div className="save-button">
            <Button onClick={(e) => onSubmit()}>Sauvegarder</Button>
          </div>
        </ModalFooter>
      </Modal>
    </div>
  );
};

AddExistingProduct.prototype = {
  getAllProduct: PropTypes.func.isRequired,
  addExistingProduct: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  allProducts: state.product.allProducts,
  refresh: state.product.refresh,
});
export default connect(StateProps, {
  getAllProduct,
  addExistingProduct,
})(withRouter(AddExistingProduct));
