/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, Suspense, useRef } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { connect, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import { getOneCardByName } from "../../../../providers/actions/card";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddCategory from "./add/AddCategory";
import AddProduit from "./add/addProduit";
import { Row, Col, Label, Form, InputGroup } from "reactstrap";
import "./card.css";
import { Container, TextField } from "@material-ui/core";
import ProductCardview from "../product/ProductCardview";
import AddExistingProduct from "./add/AddExistingProduct";
import MenuView from "../Menu/MenuView";

const Card = ({
  cateroryAction,
  getOneCardByName,
  card,
  menuAction,
  componentName,
  product,
  cardID,
}) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [ParentTabBar, setParentTabBar] = React.useState(0);
  const [Card, setCard] = React.useState();
  const dispatch = useDispatch();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeParent = (event, newValue) => {
    setParentTabBar(newValue);
  };
  useEffect(() => {
    if (!card) getOneCardByName(cardID);
  }, [getOneCardByName]);
  useEffect(() => {
    if (card) {
      dispatch({ type: "GET_CARD_CLEAR" });
      setCard(card);
    }
  }, [card]);
  useEffect(() => {
    if (cateroryAction) {
      getOneCardByName(cardID);
      dispatch({ type: "ADD_CATEGORY_CLEAR" });
    }
  }, [cateroryAction]);
  useEffect(() => {
    getOneCardByName(cardID);
    dispatch({ type: "ADD_CATEGORY_CLEAR" });
  }, [product.refresh]);
  useEffect(() => {
    if (menuAction === "ADD_MENU_SUCCESS") {
      getOneCardByName(cardID);
      dispatch({ type: "ADD_MENU_REFRESH" });
    }
  }, [menuAction]);
  return (
    <div className={classes.root}>
      <Suspense fallback={loading()}>
        <AppBar
          position="static"
          className="appBar-Parent"
          id="CardsCategoriesMenusTabBar"
        >
          <Tabs
            value={ParentTabBar}
            onChange={handleChangeParent}
            aria-label="simple tabs example"
            className="tab-parent"
          >
            <Tab label="Categories" {...a11yProps(0)} />
            <Tab label="Menus" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel
          value={ParentTabBar}
          className="parent-sous-tab"
          index={0}
          id="CardsCategoryMenu"
        >
          <div className="add-category-btn">
            <AddCategory cardID={cardID} />
          </div>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              {Card &&
                Card.category.map((category, index) => (
                  <Tab
                    label={category.name}
                    style={{
                      backgroundImage: "url(" + category.image + ")",
                    }}
                    {...a11yProps(0)}
                  />
                ))}
            </Tabs>
          </AppBar>
          {/* <Row>
            <div className="add-category-btn">
              <AddCategory cardID={cardID} />
            </div>
          </Row> */}
          <div className="tab-body">
            {Card && Card.category.length > 0 ? (
              <></>
            ) : (
              // <Row>
              //   <div className="add-category-btn">
              //     <AddCategory cardID={cardID} />
              //   </div>
              // </Row>
              <div></div>
            )}
            {Card &&
              Card.category.map((category, index) =>
                category ? (
                  <TabPanel
                    value={value}
                    index={index}
                    className="category-tab"
                  >
                    <Container>
                      {/* <Col>
                          <Row>
                            <Col>
                              <div className="image-category">
                                <img src={category.image} alt="" />
                              </div>
                            </Col>
                            <Col>
                              <Row>
                                <Label>
                                  <h5>Name :</h5> <p>{category.name}</p>
                                </Label>
                              </Row>
                              <Row>
                                <Label>
                                  <h5>Description :</h5>{" "}
                                  <p>{category.description}</p>
                                </Label>
                              </Row>
                            </Col>
                          </Row>
                        </Col> */}

                      <div
                        className="add-category-btn"
                        id="AddProductsInCategoryBtnsContainer"
                      >
                        {/* <Row>
                                                                                                         <AddCategory cardID={cardID} />
                                                                                                  </Row> */}

                        <AddProduit categoryID={category._id} />

                        <AddExistingProduct
                          currentProduct={category.product}
                          categoryID={category._id}
                        />
                      </div>

                      <hr />
                      {category.product ? (
                        <Row xs="4">
                          {category.product.map((produit, inx) => (
                            <Col xs="4" style={{ marginTop: "10px" }}>
                              <ProductCardview
                                product={produit}
                                category={category}
                                cardID={cardID}
                                fromCategory={true}
                              />
                            </Col>
                          ))}
                        </Row>
                      ) : null}
                    </Container>
                  </TabPanel>
                ) : null
              )}
          </div>
        </TabPanel>
        <TabPanel value={ParentTabBar} className="parent-sous-tab" index={1}>
          {Card && <MenuView card={Card} />}
        </TabPanel>
      </Suspense>
    </div>
  );
};
Card.prototype = {
  getOneCardByName: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  errors: state.errors,
  cateroryAction: state.category.action,
  card: state.card.card,
  product: state.product,
  menuAction: state.menu.action,
});
export default connect(StateProps, { getOneCardByName })(withRouter(Card));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: /* theme.palette.background.paper */ "#f0f0fa",
  },
}));
const loading = () => (
  <div className="animated fadeIn pt-1 text-center">Loading...</div>
);
