/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { editProduct } from "../../../../../providers/actions/product";
import {
  TextField,
  Input,
  makeStyles,
  Container,
  MenuItem,
} from "@material-ui/core";

import {
  InputGroup,
  Row,
  Label,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import _ from "lodash";
const Form = ({
  errors,
  action,
  editProduct,
  product,
  fromCategory,
  handleCloseMenu,
}) => {
  const classes = useStyles();
  const [Errors, setErrors] = useState({});
  const [ProductObject, setProductObject] = useState({
    name: product.name,
    description: product.description,
    price: product.price,
  });
  const [modal, setModal] = useState(false);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const toggle = () => setModal(!modal);
  const onChange = (e) =>
    setProductObject({ ...ProductObject, [e.target.name]: e.target.value });
  const onSubmit = () => {
    /*       var bodyFormData = new FormData()
              bodyFormData.set('name', ProductObject.name)
              bodyFormData.set('description', ProductObject.description)
              bodyFormData.set('price', ProductObject.price)
              bodyFormData.append('image', ProductObject.image) */
    /*     editProduct(product._id, bodyFormData) */
    editProduct(product._id, ProductObject);
  };
  useEffect(() => {
    if (errors) setErrors(errors);
  }, [errors]);

  return (
    <div className={classes.root}>
      {!fromCategory && (
        <MenuItem
          onClick={(e) => {
            handleCloseMenu();
            toggle();
          }}
        >
          Modifier
        </MenuItem>
      )}
      <Modal className="add-Product-modal" isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Modifier </ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col xs="6">
                <InputGroup className="mb-4">
                  <TextField
                    id="outlined-helperText"
                    label="Name"
                    variant="outlined"
                    name="name"
                    defaultValue={ProductObject.name}
                    onChange={(e) => onChange(e)}
                    error={!_.isNil(Errors.name)}
                    helperText={Errors.name}
                  />
                </InputGroup>
                <InputGroup className="mb-4">
                  <TextField
                    style={{ marginLeft: "1px" }}
                    id="outlined-helperText"
                    label="description"
                    variant="outlined"
                    name="description"
                    defaultValue={ProductObject.description}
                    onChange={(e) => onChange(e)}
                    error={!_.isNil(Errors.description)}
                    helperText={Errors.description}
                  />
                </InputGroup>
                <InputGroup className="mb-4">
                  <TextField
                    style={{ marginLeft: "1px" }}
                    id="outlined-helperText"
                    label="Price"
                    variant="outlined"
                    name="price"
                    defaultValue={ProductObject.price}
                    onChange={(e) => onChange(e)}
                    error={!_.isNil(Errors.price)}
                    helperText={Errors.price}
                  />
                </InputGroup>
                {/* 
                                                        <InputGroup className='mb-4'>
                                                               <Row>
                                                                      {' '}
                                                                      <Label sm='5' size='sm' htmlFor='filePicker' id='upload-image-filial'>
                                                                             Choose image...
                                                                      </Label>
                                                               </Row>
                                                               <Row>
                                                                      {' '}
                                                                      <div style={{ color: 'red', marginTop: '10px', marginLeft: '27px' }}>
                                                                             {Errors.image}
                                                                      </div>
                                                               </Row>

                                                               <Input
                                                                      id='filePicker'
                                                                      style={{ display: 'none' }}
                                                                      type={'file'}
                                                                      name='image'
                                                                      onChange={(e) =>
                                                                             setProductObject({
                                                                                    ...ProductObject,
                                                                                    [e.target.name]: e.target.files[0],
                                                                             })
                                                                      }
                                                               />
                                                        </InputGroup> */}
              </Col>
              <Col></Col>
            </Row>
          </Container>
        </ModalBody>
        <ModalFooter>
          <Button className="cancel-btn" onClick={toggle}>
            Annuler
          </Button>
          <Button className="save-btn" onClick={(e) => onSubmit()}>
            Sauvegarder
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};
Form.prototype = {
  editProduct: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  errors: state.errors,
  action: state.product.action,
  allProducts: state.product.allProducts,
});
export default connect(StateProps, {
  editProduct,
})(withRouter(Form));
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
