/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import './product.css'
import Form from './Form'
import View from './View'
import { getAllProduct } from '../../../../providers/actions/product'
import { Row, Col } from 'reactstrap'
const Layout = ({ allProducts, getAllProduct }) => {
       const [Products, setProducts] = useState()
       useEffect(() => {
              if (allProducts) setProducts(allProducts)
       }, [allProducts])
       useEffect(() => {
              getAllProduct()
       }, [])

       return (
              <div>
                     <Row xs='1' sm='2' md='3' className='row-add-product'>
                            <Col></Col>
                            <Col sm={{ size: 'auto', offset: 4 }}>
                                   {' '}
                                   <Form />
                            </Col>
                     </Row>

                     {Products && <View Products={Products} />}
              </div>
       )
}
Layout.prototype = {
       getAllProduct: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       errors: state.errors,
       allProducts: state.product.allProducts,
})
export default connect(StateProps, {
       getAllProduct,
})(withRouter(Layout))
