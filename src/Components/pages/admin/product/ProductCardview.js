/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Row, Col, CardFooter } from "reactstrap";
import { Container } from "@material-ui/core";
import View from "./action/View";
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));
const ProductCardview = ({
  product,
  Assign,
  allIngredient,
  ViewIngredient,
  category,
  fromCategory,
  cardID,
}) => {
  const classes = useStyles();
  const [ProductObject, setProductObject] = useState(product);
  useEffect(() => {
    setProductObject(product);
    onChangeStatus();
  }, [product]);
  const onChangeStatus = (e) => {
    if (product["ingredientObligatoire"].length === 0) {
      setProductObject({ ...ProductObject, status: "banned" });
    } else if (product) {
      product["ingredientObligatoire"].map((ing) => {
        if (ing.status === "unactive") {
          setProductObject({ ...ProductObject, status: "banned" });
        }
      });
    } else {
      setProductObject({ ...ProductObject, status: null });
    }
  };
  return (
    <div className={"product-card-view"}>
      <Card className={classes.root}>
        <CardHeader
          component={"span"}
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              {product.name}
            </Avatar>
          }
          action={
            <View
              category={category}
              product={product}
              fromCategory={fromCategory}
              cardID={cardID}
            />
          }
          title={product.name}
          // subheader={product.price}
        />
        <CardMedia
          className={classes.media}
          image={product.image}
          title={product.name}
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component={"span"}>
            {product.description}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <Container>
            {allIngredient && (
              <Row>
                <Assign allIngredient={allIngredient} prodID={product._id} />
                <ViewIngredient productIngredient={product} />
              </Row>
            )}
          </Container>
        </CardActions>
        <CardFooter id="productCardBottomInfo">
          <div id="productCardPrice">
            <h4>{product.price} €</h4>
          </div>
          {ProductObject.status === "banned" ? (
            <div id="productCardAvailabilityRed" style={{ color: "red" }}>
              <h5>Non Disponible</h5>
            </div>
          ) : (
            <div id="productCardAvailabilityGreen" style={{ color: "green" }}>
              <h5>Disponible</h5>
            </div>
          )}{" "}
        </CardFooter>
      </Card>
    </div>
  );
};

ProductCardview.propTypes = {};

export default ProductCardview;
