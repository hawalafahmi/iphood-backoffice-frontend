/* eslint-disable no-extend-native */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import { connect } from "react-redux";
import MultiSelect from "react-multi-select-component";
import { assignExistingIngredient } from "../../../../providers/actions/ingredient";
import PropTypes from "prop-types";
const AssignIngredient = ({
  assignExistingIngredient,
  prodID,
  allIngredient,
}) => {
  const [modal, setModal] = useState(false);
  const [ObligIng, setObligIng] = useState([]);
  const [OptionalIng, setOptionalIng] = useState([]);
  const [ExtraIng, setExtraIng] = useState([]);
  const [Options, setOptions] = useState([]);

  const toggle = () => {
    setObligIng([]);
    setModal(!modal);
  };
  const onSubmit = () => {
    if (ObligIng.length > 0 || OptionalIng.length > 0 || ExtraIng.length > 0) {
      let ingredientArray = {
        ingredientObligatoire: [],
        ingredientOptional: [],
        ingredientExtrat: [],
      };
      ObligIng.map((items) =>
        ingredientArray.ingredientObligatoire.push(items.value)
      );
      OptionalIng.map((items) =>
        ingredientArray.ingredientOptional.push(items.value)
      );
      ExtraIng.map((items) =>
        ingredientArray.ingredientExtrat.push(items.value)
      );
      assignExistingIngredient(prodID, ingredientArray);
      setModal(!modal);
    }
  };

  useEffect(() => {
    Options.length === 0 &&
      allIngredient &&
      allIngredient.map((ing, inx) =>
        setOptions((prevState) => [
          ...prevState,
          {
            label: ing.name,
            value: ing._id,
          },
        ])
      );
  }, [allIngredient]);

  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Button onClick={toggle} className="assign-ingredient">
        <Row>
          <Col>
            <div className="add-category-text"> Modifier Ingrédients </div>
          </Col>
        </Row>
      </Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}> Selectionner Ingrédients </ModalHeader>
        <ModalBody>
          <div>
            <MultiSelect
              options={Options}
              value={ObligIng}
              onChange={setObligIng}
              labelledBy={"Selectionner"}
              overrideStrings={{
                selectSomeItems: "Ingrédients obligatoires",
                selectAll: "Selectionner Tous",
              }}
            />
          </div>
          <div>
            <MultiSelect
              options={Options}
              value={OptionalIng}
              onChange={setOptionalIng}
              labelledBy={"Selectionner"}
              overrideStrings={{
                selectSomeItems: "Ingrédients facultatifs",
                selectAll: "Selectionner Tous",
              }}
            />
          </div>
          <div>
            <MultiSelect
              options={Options}
              value={ExtraIng}
              onChange={setExtraIng}
              labelledBy={"Selectionner"}
              overrideStrings={{
                selectSomeItems: "Ingrédients supplémentaires",
                selectAll: "Selectionner Tous",
              }}
            />
          </div>
        </ModalBody>
        <ModalFooter className="add-card-modal-button-action">
          <div className="cancel-button">
            <Button onClick={toggle}> Annuler </Button>
          </div>
          <div className="save-button">
            <Button onClick={(e) => onSubmit()}> Sauvegarder </Button>
          </div>
        </ModalFooter>
      </Modal>
    </div>
  );
};
AssignIngredient.prototype = {
  assignExistingIngredient: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  assignExistingIngredient,
})(AssignIngredient);
/* Array.prototype.remove = function () {
       var what,
              a = arguments,
              L = a.length,
              ax
       while (L && this.length) {
              what = a[--L]
              while ((ax = this.indexOf(what)) !== -1) {
                     this.splice(ax, 1)
              }
       }
       return this
}
var ary = ['three', 'seven', 'eleven'];

ary.remove('seven');
 */
