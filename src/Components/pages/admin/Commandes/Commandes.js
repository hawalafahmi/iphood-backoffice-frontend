import React from "react";
import "./Commandes.css";
import { connect } from "react-redux";
import CommandesImg from "../../../../assets/img/brand/orders.jpeg";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "reactstrap";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];
const Commandes = ({}) => {
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="center">Date</TableCell>
            <TableCell align="center">Ètat</TableCell>
            <TableCell align="center">Client</TableCell>

            <TableCell align="center">Montant (€)</TableCell>
            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.calories}
              </TableCell>
              <TableCell align="center">27 - 11 - 2022</TableCell>
              <TableCell align="center">
                <Button disabled color="success">
                  En cours
                </Button>
              </TableCell>

              <TableCell align="center">
                <Button color="">John Red</Button>
              </TableCell>
              <TableCell align="center">{row.protein}</TableCell>
              <TableCell align="right">
                <IconButton aria-label="delete" className={classes.margin}>
                  <MoreVertIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const StateProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});
export default connect(StateProps, {
  Commandes,
})(Commandes);
