/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import MultiSelect from 'react-multi-select-component'
import { addExistingCategoryToMenu } from '../../../../providers/actions/menu'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
const AddExistCategory = ({ category, addExistingCategoryToMenu, menuID }) => {
       const [modal, setModal] = useState(false)
       const [selected, setSelected] = useState([])
       const [Options, setOptions] = useState([])
       useEffect(() => {
              Options.length === 0 &&
                     category &&
                     category.map((cat, inx) => setOptions((prevState) => [...prevState, { label: cat.name, value: cat._id }]))
       }, [category])
       const toggle = () => {
              setSelected([])
              setModal(!modal)
       }
       const onSubmit = () => {
              if (selected.length > 0) {
                     let productArray = []
                     selected.map((items) => productArray.push(items.value))
                     addExistingCategoryToMenu(menuID, productArray)
                     toggle()
              }
       }
       return (
              <div>
                     <Button color='danger' className='add-category-menu' onClick={toggle}>
                            Add category
                     </Button>
                     <Modal isOpen={modal} toggle={toggle} className='modal-add-existing-category'>
                            <ModalHeader toggle={toggle}>Modal title</ModalHeader>
                            <ModalBody>
                                   <div>
                                          <MultiSelect options={Options} value={selected} onChange={setSelected} labelledBy={'Select'} />
                                   </div>
                            </ModalBody>
                            <ModalFooter className='add-card-modal-button-action'>
                                   <div className='cancel-button'>
                                          <Button onClick={toggle}>Cancel</Button>
                                   </div>
                                   <div className='save-button'>
                                          <Button onClick={(e) => onSubmit()}>save</Button>
                                   </div>
                            </ModalFooter>
                     </Modal>
              </div>
       )
}

AddExistCategory.prototype = {
       addExistingCategoryToMenu: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       menu: state.menu,
})
export default connect(StateProps, {
       addExistingCategoryToMenu,
})(withRouter(AddExistCategory))
