import React, { Component, useState, useEffect } from "react";
import { connect } from "react-redux";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import PropTypes from "prop-types";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme, makeStyles } from "@material-ui/core/styles";
import {
  removeProductFromMenu,
  editProductMenu,
} from "../../../../../providers/actions/productMenu";
import { IconButton, TextField, Container } from "@material-ui/core";
import _ from "lodash";
import { Col, Row, InputGroup, Input, Label } from "reactstrap";
import ImgView from "../../Card/add/ImgView";
const Product = ({
  product,
  removeProductFromMenu,
  menu,
  categoryMenu,
  editProductMenu,
  errors,
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [EditModal, setEditModal] = React.useState(false);
  const [DeleteModal, setDeleteModal] = React.useState(false);
  const theme = useTheme();
  const [Errors, setErrors] = useState({});

  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [ProductObject, setProductObject] = useState(product);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleModalEdit = () => setEditModal(!EditModal);
  const handleModalAlertDelete = () => setDeleteModal(!DeleteModal);

  const onChange = (e) => {
    setProductObject({ ...ProductObject, [e.target.name]: e.target.value });
  };
  const onSubmit = () => {
    var bodyFormData = new FormData();
    bodyFormData.set("name", ProductObject.name);
    bodyFormData.set("description", ProductObject.description);
    bodyFormData.set("price", ProductObject.price);
    bodyFormData.append("image", ProductObject.image);
    editProductMenu(menu._id, product._id, bodyFormData);
  };
  useEffect(() => {
    if (errors) setErrors(errors);
  }, [errors]);
  return (
    <div>
      <IconButton
        component={"span"}
        aria-label="settings"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>

      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem
          onClick={(e) => {
            handleClose();
            handleModalEdit();
          }}
        >
          Modifier
        </MenuItem>
        <MenuItem
          onClick={(e) => {
            handleClose();
            handleModalAlertDelete();
          }}
        >
          Supprimer
        </MenuItem>
      </Menu>
      {/* Modal Edit Product */}
      <Dialog
        fullScreen={fullScreen}
        open={EditModal}
        onClose={handleModalEdit}
      >
        <DialogTitle id="responsive-dialog-title">
          {"Edit : " + ProductObject.name}
        </DialogTitle>
        <DialogContent>
          <Row>
            <Col>
              <InputGroup>
                <TextField
                  className="edit-product-menu"
                  id="outlined-helperText"
                  label="Nom"
                  variant="outlined"
                  name="name"
                  defaultValue={ProductObject.name}
                  onChange={(e) => onChange(e)}
                  error={!_.isNil(Errors.name)}
                  helperText={Errors.name}
                />
              </InputGroup>

              <InputGroup>
                <TextField
                  className="edit-product-menu"
                  style={{ marginLeft: "1px", marginTop: "10px" }}
                  id="outlined-helperText"
                  label="Description"
                  variant="outlined"
                  name="description"
                  defaultValue={ProductObject.description}
                  onChange={(e) => onChange(e)}
                  error={!_.isNil(Errors.description)}
                  helperText={Errors.description}
                />
              </InputGroup>
              <InputGroup className="mb-4">
                <TextField
                  className="edit-product-menu"
                  style={{ marginLeft: "1px", marginTop: "10px" }}
                  id="outlined-helperText"
                  label="Prix"
                  variant="outlined"
                  name="price"
                  defaultValue={ProductObject.price}
                  onChange={(e) => onChange(e)}
                  error={!_.isNil(Errors.price)}
                  helperText={Errors.price}
                />
              </InputGroup>

              <InputGroup className="mb-4">
                <Row>
                  {" "}
                  <Label
                    sm="5"
                    size="sm"
                    htmlFor="filePicker"
                    id="upload-image-filial"
                  >
                    Choisir image...
                  </Label>
                </Row>
                <Row>
                  {" "}
                  <div
                    style={{
                      color: "red",
                      marginTop: "10px",
                      marginLeft: "27px",
                    }}
                  >
                    {Errors.image}
                  </div>
                </Row>

                <Input
                  id="filePicker"
                  style={{ display: "none" }}
                  type={"file"}
                  name="image"
                  onChange={(e) =>
                    setProductObject({
                      ...ProductObject,
                      [e.target.name]: e.target.files[0],
                    })
                  }
                />
              </InputGroup>
            </Col>
          </Row>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleModalEdit} color="primary">
            Annuler
          </Button>
          <Button
            onClick={(e) => {
              handleModalEdit();
              onSubmit();
            }}
            color="primary"
            autoFocus
          >
            Sauvegarder
          </Button>
        </DialogActions>
      </Dialog>
      {/* Modal Edit Product */}

      {/* Alert delee modal  */}
      <Dialog
        open={DeleteModal}
        onClose={handleModalAlertDelete}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Êtes-vous sûr ?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            ce produit ne sera supprimé que de ce menu si vous souhaitez le
            supprimer complètement, vous devez le supprimer de la page
            principale du produit
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleModalAlertDelete} color="primary">
            Refuser
          </Button>
          <Button
            onClick={(e) => {
              handleModalAlertDelete();
              removeProductFromMenu(menu._id, categoryMenu._id, product._id);
            }}
            color="primary"
            autoFocus
          >
            Accepter
          </Button>
        </DialogActions>
      </Dialog>
      {/* Alert delee modal  */}
    </div>
  );
};

const mapStateToProps = (state) => ({
  errors: state.errors,
});

const mapDispatchToProps = {
  removeProductFromMenu,
  editProductMenu,
};
Product.prototype = {
  removeProductFromMenu: PropTypes.func.isRequired,
  editProductMenu: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, mapDispatchToProps)(Product);
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));
