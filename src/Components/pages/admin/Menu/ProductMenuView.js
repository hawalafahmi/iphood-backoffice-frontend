/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Row, CardFooter } from "reactstrap";
import { Container } from "@material-ui/core";
import Product from "./actions/Product";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));
const ProductCardview = ({
  product,
  Assign,
  allIngredient,
  ViewIngredient,
  menu,
  categoryMenu,
}) => {
  const classes = useStyles();
  const [ProductObject, setProductObject] = useState(product);
  useEffect(() => {
    setProductObject(product);
    onChangeStatus();
  }, [product]);
  const onChangeStatus = (e) => {
    if (product["ingredientObligatoire"].length != 0) {
      setProductObject({ ...ProductObject, status: "banned" });
    } else if (product) {
      product["ingredientObligatoire"].map((ing) => {
        if (ing.status === "unactive") {
          setProductObject({ ...ProductObject, status: "banned" });
        }
      });
    } else {
      setProductObject({ ...ProductObject, status: null });
    }
  };
  return (
    <div className={"product-card-view"}>
      <Card className={classes.root}>
        <CardHeader
          component={"span"}
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              {product.name}
            </Avatar>
          }
          action={
            <Product
              product={product}
              menu={menu}
              categoryMenu={categoryMenu}
            />
          }
          title={product.name}
          subheader={product.price}
        />
        <CardMedia
          className={classes.media}
          image={product.image}
          title="Paella dish"
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component={"span"}>
            {product.description}
          </Typography>
        </CardContent>
        <CardActions disableSpacing></CardActions>
        <CardFooter>
          {ProductObject.status === "banned" ? (
            <div style={{ color: "red" }}>Non Disponible</div>
          ) : (
            <div style={{ color: "green" }}>Disponible</div>
          )}{" "}
        </CardFooter>
      </Card>
    </div>
  );
};

ProductCardview.propTypes = {};

export default ProductCardview;
