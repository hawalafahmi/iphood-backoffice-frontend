import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Row, Col, Label } from 'reactstrap'
import { TextField } from '@material-ui/core'
import _ from 'lodash'
import PropTypes from 'prop-types'
import { useDispatch, connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { addNewMenu } from '../../../../providers/actions/menu'
const AddMenu = ({ card, addNewMenu, errors, menu }) => {
       const [MENU, setMENU] = useState({ name: '', description: '', relatedTo: card._id })
       const [modal, setModal] = useState(false)
       const [Errors, setErrors] = useState({})

       const toggle = () => {
              dispatch({ type: 'CLEAR_ERRORS' })
              setModal(!modal)
       }
       const dispatch = useDispatch()
       const onChange = (e) => setMENU({ ...MENU, [e.target.name]: e.target.value })
       const onSubmit = () => {
              setModal(false)
              addNewMenu(MENU)
       }
       useEffect(() => {
              if (errors) setErrors(errors)
       }, [errors])
       /*      useEffect(() => {
              if (menu.action === 'ADD_MENU_SUCCESS') 
       }, [menu.action]) */
       return (
              <div>
                     <Button color='danger' onClick={toggle}>
                            Create new Menu
                     </Button>
                     <Modal isOpen={modal} toggle={toggle} className='modal-add-menu'>
                            <ModalHeader toggle={toggle}>Create new Menu</ModalHeader>
                            <ModalBody>
                                   <Container>
                                          <Row>
                                                 <Col xs='4'>
                                                        <Label style={{ marginTop: '17px' }}>Menu name </Label>
                                                 </Col>
                                                 <Col xs='6'>
                                                        <TextField
                                                               id='standard-basic'
                                                               label='name'
                                                               name='name'
                                                               onChange={(e) => onChange(e)}
                                                               error={!_.isNil(Errors.name)}
                                                               helperText={Errors.name}
                                                        />
                                                 </Col>
                                          </Row>
                                          <Row>
                                                 <Col xs='4'>
                                                        <Label style={{ marginTop: '17px' }}> description </Label>
                                                 </Col>
                                                 <Col xs='6'>
                                                        <TextField
                                                               id='standard-basic'
                                                               label='description'
                                                               name='description'
                                                               onChange={(e) => onChange(e)}
                                                               error={!_.isNil(Errors.description)}
                                                               helperText={Errors.description}
                                                        />
                                                 </Col>
                                          </Row>
                                   </Container>
                            </ModalBody>
                            <ModalFooter className='add-menu-modal-button-action'>
                                   <div className='cancel-button'>
                                          <Button onClick={toggle}>Cancel</Button>
                                   </div>
                                   <div className='save-button'>
                                          <Button onClick={(e) => onSubmit()}>save</Button>
                                   </div>
                            </ModalFooter>
                     </Modal>
              </div>
       )
}
AddMenu.prototype = {
       addNewMenu: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       errors: state.errors,
       menu: state.menu,
})
export default connect(StateProps, { addNewMenu })(withRouter(AddMenu))
