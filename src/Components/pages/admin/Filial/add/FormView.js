import React, { useState } from "react";
import {
  Card,
  CardTitle,
  CardBody,
  Container,
  CardFooter,
  Row,
  Label,
  Col,
} from "reactstrap";
const FormView = ({ FilialObject }) => {
  const [img, setimg] = useState(null);
  if (FilialObject.image) {
    const reader = new FileReader();

    reader.readAsDataURL(FilialObject.image);
    reader.onloadend = () => {
      setimg(reader.result);
    };
  }
  return (
    <Card className="preview-card">
      <Container>
        {" "}
        <CardTitle>
          <h3>Aperçu</h3>
        </CardTitle>
        <CardBody>
          {img ? (
            <img alt="" src={img} />
          ) : (
            <img alt="" src="https://via.placeholder.com/150" />
          )}
        </CardBody>
        <CardFooter>
          <Row>
            <Col>
              <Label>
                <h4>Nom:</h4>
              </Label>
            </Col>
            <Col>{FilialObject.name}</Col>
          </Row>
          <Row>
            <Col>
              <Label>
                <h4>Adresse:</h4>
              </Label>
            </Col>
            <Col>{FilialObject.address}</Col>
          </Row>
          <Row>
            <Col>
              <Label>
                <h4>Téléphone:</h4>
              </Label>
            </Col>
            <Col>{FilialObject.phone}</Col>
          </Row>
        </CardFooter>
      </Container>
    </Card>
  );
};

export default FormView;
