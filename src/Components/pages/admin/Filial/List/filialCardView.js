import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import clsx from "clsx";
import EditFilial from "./EditFilial";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import { Button, Collapse, Icon } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AsignCard from "../assignCard/AssignCard";
import { connect } from "react-redux";
import "./filialList.css";

import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { DELETE_FILIAL_CARD, GET_ERRORS } from "../../../../../providers/type";
import { loadUser } from "../../../../../providers/actions/auth";
import API from "../../../../../utils/API";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

// const removeFilial = (filialID) => async (dispatch) => {
//   try {
//     const res = await API.delete(`/filial/${filialID}`);
//     dispatch(loadUser());
//     dispatch({
//       type: DELETE_FILIAL_CARD,
//       payload: res.data.data,
//     });
//   } catch (_err) {
//     const errors = _err.response.data.error;
//     dispatch({
//       type: GET_ERRORS,
//       payload: errors,
//     });
//   }
// };

async function removeFilial(filialID) {
  console.log("deleting filial");
  try {
    const res = await API.delete("/filial/" + filialID);
  } catch (_err) {
    const errors = _err.response.data.error;
  }
}

function FilailCardView({ restaurant, filial, cards }) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card className={classes.root} id="FilialCard">
      <CardHeader
        avatar={<img alt="" src={restaurant.image} id="filial-preview" />}
        action={
          <IconButton
            component={"span"}
            aria-label="settings"
            onClick={handleClickOpen}
          >
            <DeleteIcon />
          </IconButton>
        }
        title={filial.name}
        //  subheader={filial.phone}
      />
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {"Êtes-vous sûr ?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <div>Supprimer Filiale?</div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={(e) => {
              handleClose();
            }}
            color="primary"
          >
            Refuser
          </Button>
          <Button
            onClick={(e) => {
              handleClose();
              removeFilial(filial._id);
              console.log("deleting...");
            }}
            color="primary"
          >
            Accepter
          </Button>
        </DialogActions>
      </Dialog>
      <CardMedia
        className={classes.media}
        image={filial.image}
        title={filial.name}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="span">
          {filial.phone}
        </Typography>
        <br />
        <Typography variant="body2" color="textSecondary" component="span">
          {filial.address}
        </Typography>
      </CardContent>
      <CardActions disableSpacing style={{ display: "block" }}>
        <Button
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          {" "}
          <Icon
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
          >
            <div className="expand-icon">
              <ExpandMoreIcon />
            </div>
          </Icon>
          Modifier
        </Button>
        <Button style={{ float: "right" }}>
          {" "}
          <AsignCard filial={filial} allCards={cards} />
        </Button>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <EditFilial oldFilial={filial} setExpanded={setExpanded} />
        </CardContent>
      </Collapse>
    </Card>
  );
}
FilailCardView.propTypes = {};
const StateProps = (state) => ({
  cards: state.card.currentCards,
});
export default connect(StateProps, null)(withRouter(FilailCardView));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
