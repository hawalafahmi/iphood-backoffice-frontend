/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import Board, { moveCard } from "@lourenci/react-kanban";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import PropTypes from "prop-types";
import "@lourenci/react-kanban/dist/styles.css";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { addCardToFilial } from "../../../../../providers/actions/filial";
import { words } from "lodash";
const AssignCard = ({ filial, allCards, addCardToFilial }) => {
  const board = {
    columns: [
      {
        id: 1,
        title: "Filiale",
        cards: [],
      },
      {
        id: 2,
        title: "Cartes",
        cards: [],
      },
    ],
  };

  const [DragAndDropBoard, setBoard] = useState(board);
  const [modal, setModal] = useState(false);

  const handleCardMove = (_card, source, destination) => {
    const updatedBoard = moveCard(DragAndDropBoard, source, destination);
    setBoard(updatedBoard);
  };

  const toggle = () => setModal(!modal);
  const onSubmit = () => {
    let productArray = [];
    DragAndDropBoard.columns[0].cards.length > 0 &&
      DragAndDropBoard.columns[0].cards.map((card) =>
        productArray.push(card.cardID)
      );
    addCardToFilial(filial._id, productArray);
    setModal(false);
  };
  var idsFilter = [];
  filial &&
    filial.card.map((filialCards, index) => {
      idsFilter = [...idsFilter, filialCards._id];
      board.columns[0].cards.push({
        id: filialCards._id,
        title: filialCards.name,
        cardID: filialCards._id,
        description: filialCards.description,
      });
    });

  allCards &&
    allCards.map(
      (card, index) =>
        !idsFilter.includes(card._id) &&
        board.columns[1].cards.push({
          id: card._id,
          title: card.name,
          cardID: card._id,
        })
    );
  useEffect(() => {
    if (modal) setBoard(board);
  }, [modal]);
  return (
    <div className="assign-body">
      <Button color="danger" onClick={toggle}>
        Gérer cartes
      </Button>
      <div className="assign-modal-body">
        <Modal
          isOpen={modal}
          toggle={toggle}
          backdrop="static"
          className="assgin_modal"
        >
          <ModalHeader toggle={toggle}>
            Assigner cartes à : {filial.name}
          </ModalHeader>
          <ModalBody>
            <Board onCardDragEnd={handleCardMove} disableColumnDrag>
              {DragAndDropBoard}
            </Board>{" "}
          </ModalBody>
          <ModalFooter className="add-card-modal-button-action">
            <div className="cancel-button">
              <Button onClick={toggle}>Annuler</Button>
            </div>
            <div className="save-button">
              <Button onClick={(e) => onSubmit()}>Sauvegarder</Button>
            </div>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
};

AssignCard.propTypes = {
  addCardToFilial: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  cards: state.card.currentCards,
});
export default connect(StateProps, {
  addCardToFilial,
})(withRouter(AssignCard));
