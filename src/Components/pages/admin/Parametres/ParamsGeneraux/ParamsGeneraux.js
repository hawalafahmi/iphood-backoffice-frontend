import React from "react";

import { connect } from "react-redux";

const ParamsGeneraux = ({}) => {
  return <div className="app flex-row align-items-center login-body">Test</div>;
};

const StateProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});
export default connect(StateProps, {
  ParamsGeneraux,
})(ParamsGeneraux);
