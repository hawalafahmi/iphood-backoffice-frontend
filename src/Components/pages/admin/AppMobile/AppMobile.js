import React from "react";
import "./AppMobile.css";

import { connect } from "react-redux";
import AppMobileImg from "../../../../assets/img/brand/mobileapp.png";
const AppMobile = ({}) => {
  return (
    <div>
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.
      </div>
      <a href="https://play.google.com/store/apps/details?id=pro.screenflex">
        Visiter
      </a>
      <h3>Comment paramétrer vos applications mobiles :</h3>
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.
      </div>
      <img src={AppMobileImg}></img>
    </div>
  );
};

const StateProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});
export default connect(StateProps, {
  AppMobile,
})(AppMobile);
