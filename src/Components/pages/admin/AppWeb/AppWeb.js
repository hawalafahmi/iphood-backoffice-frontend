import React from "react";
import "./AppWeb.css";
import { connect } from "react-redux";
import AppWebImg from "../../../../assets/img/brand/webapp.jpg";
const AppWeb = ({}) => {
  return (
    <div>
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.
      </div>
      <a href="http://iphood-client.carthage-solutions.com/">Visiter</a>
      <h3>Comment paramétrer votre site web :</h3>
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.
      </div>
      <img src={AppWebImg}></img>
    </div>
  );
};

const StateProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});
export default connect(StateProps, {
  AppWeb,
})(AppWeb);
