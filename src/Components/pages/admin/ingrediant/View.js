import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Actions from "./Actions";
import { Chip } from "@material-ui/core";

import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  table: {
    minWidth: 650,
  },
}));

export const View = ({ IngredientObject }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  var rows = [];
  IngredientObject.map((oneIngredient) =>
    rows.push(
      createData(
        oneIngredient.name,
        oneIngredient.pickedBy,
        oneIngredient.status,
        oneIngredient.createdAt,
        oneIngredient.price,
        <Actions ingID={oneIngredient._id} currentIngredient={oneIngredient} />
      )
    )
  );
  return (
    <div className="ingredient-table-view">
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Ingrédient</TableCell>
              <TableCell></TableCell>
              <TableCell align="center">Associé au produit</TableCell>
              <TableCell align="center">Statut</TableCell>
              <TableCell align="center">Crée à</TableCell>
              <TableCell align="center">Prix</TableCell>
              <TableCell align="right">actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell component="th" scope="row" align="left">
                  <img
                    src="https://cdn-icons-png.flaticon.com/512/3143/3143643.png"
                    id="ingredientImg"
                  ></img>
                </TableCell>
                <TableCell component="th" scope="row" align="center">
                  <button
                    type="button"
                    onClick={handleOpen}
                    id="seeProductsIngredients"
                  >
                    Voir
                  </button>
                  <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                      timeout: 500,
                    }}
                  >
                    <Fade in={open}>
                      <div className={classes.paper}>
                        {row.pickedBy &&
                          row.pickedBy.map((product) => (
                            <div>{product.name} </div>
                          ))}
                      </div>
                    </Fade>
                  </Modal>
                </TableCell>
                <TableCell component="th" scope="row" align="center">
                  {row.status === "active" ? (
                    <Chip className="active_tag" label="Activé" />
                  ) : (
                    <Chip className="unactive_tag" label="Désactivé" />
                  )}
                </TableCell>
                <TableCell align="center">{row.createdAt}</TableCell>
                <TableCell align="center">{row.price}</TableCell>
                <TableCell align="right">{row.actions}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default connect(null, null)(View);
// const useStyles = makeStyles({
//   table: {
//     minWidth: 650,
//   },
// });

const createData = (name, pickedBy, status, createdAt, price, actions) => {
  return { name, pickedBy, status, createdAt, price, actions };
};
