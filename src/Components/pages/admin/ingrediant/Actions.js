/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { IconButton } from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme, makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

import {
  removeIngredient,
  EditIngredient,
} from "../../../../providers/actions/ingredient";

import Tooltip from "@material-ui/core/Tooltip";

const Actions = ({
  ingID,
  removeIngredient,
  currentIngredient,
  actionsIng,
  EditIngredient,
}) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles();
  const [Ingredient, setIngredient] = React.useState({ name: "", price: "" });
  const onChange = (e) =>
    setIngredient({ ...Ingredient, [e.target.name]: e.target.value });
  const onSubmit = (e) => EditIngredient(ingID, Ingredient);
  const changeStatus = (e) => {
    EditIngredient(ingID, { status: e });
    setOpenConfirm(false);
  };

  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const handleModalClickOpen = () => setOpen(true);
  const handleModalClose = () => {
    setOpen(false);
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };
  useEffect(() => {
    if (actionsIng === "EDIT_INGREDIENT_SUCCESS") {
      dispatch({ type: "ADD_INGREDIENT_CLEAR" });
      handleModalClose();
    }
  }, [actionsIng]);
  return (
    <div>
      <IconButton
        component={"span"}
        aria-label="settings"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem className="bold-actions" onClick={handleModalClickOpen}>
          Modifier
        </MenuItem>
        {currentIngredient.pickedBy.length > 0 ? (
          <Tooltip title={longText} classes={{ tooltip: classes.noMaxWidth }}>
            <MenuItem className="bold-actions">delete</MenuItem>
          </Tooltip>
        ) : (
          <MenuItem
            className="bold-actions"
            onClick={(e) => removeIngredient(ingID)}
          >
            Supprimer
          </MenuItem>
        )}
        {currentIngredient.status === "active" ? (
          currentIngredient.pickedBy.length > 0 ? (
            <MenuItem
              className="bold-actions unactivate"
              onClick={handleClickOpenConfirm}
            >
              Désactiver
            </MenuItem>
          ) : (
            <MenuItem
              className="bold-actions unactivate"
              onClick={(e) => changeStatus("unactive")}
            >
              Désactiver
            </MenuItem>
          )
        ) : (
          <MenuItem
            className="bold-actions activate"
            onClick={(e) => changeStatus("active")}
          >
            activer
          </MenuItem>
        )}
      </Menu>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Créer un nouvel ingrédient"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="outlined-basic"
                name="name"
                label="Nom"
                defaultValue={currentIngredient.name}
                variant="outlined"
                onChange={(e) => onChange(e)}
              />
              <TextField
                id="outlined-basic"
                name="price"
                label="Prix"
                defaultValue={currentIngredient.price}
                variant="outlined"
                onChange={(e) => onChange(e)}
              />
            </form>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleModalClose} color="primary">
            Annuler
          </Button>
          <Button onClick={(e) => onSubmit()} color="primary" autoFocus>
            Sauvegarder
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Êtes-vous sûr ?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Cet ingrédient est utilisé comme élément obligatoire dans d'autres
            produits si vous le désactivez, vous affecterez le produit.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirm} color="primary">
            Refuser
          </Button>
          <Button
            onClick={(e) => changeStatus("unactive")}
            color="primary"
            autoFocus
          >
            Accepter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
Actions.prototype = {
  removeIngredient: PropTypes.func.isRequired,
  EditIngredient: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  actionsIng: state.ingredient.action,
});
export default connect(StateProps, {
  removeIngredient,
  EditIngredient,
})(Actions);
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25em",
    },
  },
  button: {
    margin: theme.spacing(1),
  },
  customWidth: {
    maxWidth: 500,
  },
  noMaxWidth: {
    maxWidth: "none",
  },
}));
const longText = `
This ingredient is used  by other product .
make sure you remove it first.
`;
