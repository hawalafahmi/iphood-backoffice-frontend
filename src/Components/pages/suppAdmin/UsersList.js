import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Register from './registerButton/ModalRegister'
import Table from './datatable/DataTable'
import { getAllAdmins } from '../../../providers/actions/admin'
import './userList.css'
const UsersList = ({ getAllAdmins, list }) => {
    const [adminList, setadminList] = useState(null)
    useEffect(() => {
        if (!list) getAllAdmins()
    }, [getAllAdmins, list])
    useEffect(() => {
        setadminList(list)
    }, [list])
    return (
        <div className='dataTable-containers'>
            <Table adminList={adminList} />
            <div className='floating-register-button'>
                <Register />
            </div>
        </div>
    )
}

UsersList.propTypes = {
    getAllAdmins: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
    list: state.admin.list,
})

export default connect(StateProps, { getAllAdmins })(UsersList)
