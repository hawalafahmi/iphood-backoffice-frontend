import React, { useState } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { makeStyles } from '@material-ui/core/styles'
import AddIcon from '@material-ui/icons/Add.js'
import Fab from '@material-ui/core/Fab'
import Register from '../../../auth/register/Register'
import { useDispatch } from 'react-redux'
const useStyles = makeStyles((theme) => ({
     root: {
          '& > *': {
               margin: theme.spacing(1),
          },
     },
     extendedIcon: {
          marginRight: theme.spacing(1),
     },
}))
const ModalRegister = (props) => {
     const { className } = props
     const Dispatch = useDispatch()

     const [modal, setModal] = useState(false)

     const toggle = () => {
          Dispatch({ type: 'CLEAR_ERRORS' })
          setModal(!modal)
     }
     const classes = useStyles()
     return (
          <div className={classes.root}>
               <Fab color='secondary' aria-label='add' onClick={toggle}>
                    <AddIcon />
               </Fab>
               <Modal isOpen={modal} toggle={toggle} className={className}>
                    <ModalHeader toggle={toggle}>Register</ModalHeader>
                    <ModalBody>
                         <Register toggle={toggle} />
                    </ModalBody>
                    <ModalFooter></ModalFooter>
               </Modal>
          </div>
     )
}

export default ModalRegister
