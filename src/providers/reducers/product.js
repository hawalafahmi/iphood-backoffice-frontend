import { ADD_PRODUCT_REFRESH, ADD_PRODUCT_SUCCESS, ADD_PRODUCT_CLEAR, GET_PRODUCT_SUCCESS } from '../type'

const initialState = {
       card: null,
       category: null,
       action: null,
       updateedFilial: null,
       refresh: false,
       allProducts: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case ADD_PRODUCT_SUCCESS:
                     return {
                            ...state,
                            category: payload,
                            product: payload.product,
                            action: 'ADD_PRODUCT_SUCCESS',
                            refresh: true,
                     }

              case GET_PRODUCT_SUCCESS:
                     return {
                            ...state,
                            allProducts: payload,
                     }

              case ADD_PRODUCT_CLEAR:
                     return {
                            ...state,
                            action: null,
                     }
              case ADD_PRODUCT_REFRESH:
                     return {
                            ...state,
                            refresh: false,
                     }

              default:
                     return state
       }
}
