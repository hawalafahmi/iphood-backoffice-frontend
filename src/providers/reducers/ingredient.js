import {
       ADD_INGREDIENT_CLEAR,
       ADD_INGREDIENT_REFRESH,
       ADD_INGREDIENT_SUCCESS,
       GET_INGREDIENT_CLEAR,
       EDIT_INGREDIENT_SUCCESS,
       GET_INGREDIENT_SUCCESS,
} from '../type'
const initialState = {
       all: null,
       new: null,
       action: null,
       edit: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case ADD_INGREDIENT_SUCCESS:
                     return {
                            ...state,
                            new: payload,
                            action: 'ADD_INGREDIENT_SUCCESS',
                     }
              case GET_INGREDIENT_SUCCESS:
                     return {
                            ...state,
                            all: payload,
                            action: 'GET_INGREDIENT_SUCCESS',
                     }
              case EDIT_INGREDIENT_SUCCESS:
                     return {
                            ...state,
                            edit: payload,
                            action: 'EDIT_INGREDIENT_SUCCESS',
                     }

              case ADD_INGREDIENT_CLEAR:
                     return {
                            ...state,
                            action: null,
                            new: null,
                     }

              default:
                     return state
       }
}
