export const AUTH_ERROR = "AUTH_ERROR";
export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const CLEAR_REGISTER = "CLEAR_REGISTER";
export const USER_LOGIN_SUCCESS = "USER_LOGIN_SUCCESS";
export const USER_LOGIN_FAILED = "USER_LOGIN_FAILED";
export const USER_LOADED = "USER_LOADED";
export const USER_FILIALS_LOAD = "USER_FILIALS_LOAD";
export const USER_CARDS_LOAD = "USER_CARDS_LOAD";
export const USER_LOGOUT = "USER_LOGOUT";

export const CLEAR_PROFILE = "CLEAR_PROFILE";

export const ADMIN_GET_ALL_SUCCESS = "ADMIN_GET_ALL_SUCCESS";
export const ADMIN_GET_ALL_FAIL = "ADMIN_GET_ALL_FAIL";
export const ADMIN_GET_ALL_CLEAR = "ADMIN_GET_ALL_CLEAR";

export const ADMIN_GET_ID_SUCCESS = "ADMIN_GET_ID_SUCCESS";
export const ADMIN_GET_ID_CLEAR = "ADMIN_GET_ID_CLEAR";
export const ADMIN_GET_ID_FAIL = "ADMIN_GET_ID_FAIL";

export const ADMIN_UPDATE_SUCCESS = "ADMIN_UPDATE_SUCCESS";
export const ADMIN_UPDATE_CLEAR = "ADMIN_UPDATE_CLEAR";
export const ADMIN_UPDATE_FAIL = "ADMIN_UPDATE_FAIL";

export const ADD_REST_SUCCESS = "ADD_REST_SUCCESS";
export const ADD_REST_CLEAR = "ADD_REST_CLEAR";
export const ADD_REST_FAIL = "ADD_REST_FAIL";

export const ADD_FILIAL_SUCCESS = "ADD_FILIAL_SUCCESS";
export const UPDATE_FILIAL_SUCCESS = "UPDATE_FILIAL_SUCCESS";
export const UPDATE_FILIAL_CLEAR = "UPDATE_FILIAL_CLEAR";
export const ADD_FILIAL_CLEAR = "ADD_FILIAL_CLEAR";
export const ADD_FILIAL_FAIL = "ADD_FILIAL_FAIL";
export const ADD_FILIAL_CARD = "ADD_FILIAL_CARD";
export const DELETE_FILIAL_CARD = "DELETE_FILIAL_CARD";

export const ADD_CARD_SUCCESS = "ADD_CARD_SUCCESS";
export const ADD_CARD_CLEAR = "ADD_CARD_CLEAR";
export const GET_CARD_SUCCESS = "GET_CARD_SUCCESS";
export const GET_CARD_CLEAR = "GET_CARD_CLEAR";

export const ADD_CATEGORY_REFRESH = "ADD_CATEGORY_REFRESH";
export const ADD_CATEGORY_SUCCESS = "ADD_CATEGORY_SUCCESS";
export const ADD_CATEGORY_CLEAR = "ADD_CATEGORY_CLEAR";
export const GET_CATEGORY_SUCCESS = "GET_CATEGORY_SUCCESS";
export const GET_CATEGORY_CLEAR = "GET_CATEGORY_CLEAR";

export const ADD_MENU_REFRESH = "ADD_MENU_REFRESH";
export const ADD_MENU_SUCCESS = "ADD_MENU_SUCCESS";
export const ADD_MENU_CATEGORY_SUCCESS = "ADD_MENU_CATEGORY_SUCCESS";
export const ADD_MENU_CLEAR = "ADD_MENU_CLEAR";
export const GET_MENU_SUCCESS = "GET_MENU_SUCCESS";
export const GET_MENU_CLEAR = "GET_MENU_CLEAR";
export const GET_ONE_MENU_CLEAR = "GET_ONE_MENU_CLEAR";

export const ADD_PRODUCT_REFRESH = "ADD_PRODUCT_REFRESH";
export const ADD_PRODUCT_SUCCESS = "ADD_PRODUCT_SUCCESS";
export const ADD_PRODUCT_CLEAR = "ADD_PRODUCT_CLEAR";
export const GET_PRODUCT_SUCCESS = "GET_PRODUCT_SUCCESS";
export const GET_PRODUCT_CLEAR = "GET_PRODUCT_CLEAR";

export const ADD_INGREDIENT_REFRESH = "ADD_INGREDIENT_REFRESH";
export const ADD_INGREDIENT_SUCCESS = "ADD_INGREDIENT_SUCCESS";
export const ADD_INGREDIENT_CLEAR = "ADD_INGREDIENT_CLEAR";
export const GET_INGREDIENT_SUCCESS = "GET_INGREDIENT_SUCCESS";
export const GET_INGREDIENT_CLEAR = "GET_INGREDIENT_CLEAR";
export const EDIT_INGREDIENT_SUCCESS = "EDIT_INGREDIENT_SUCCESS";
export const REMOVE_INGREDIENT_SUCCESS = "REMOVE_INGREDIENT_SUCCESS";

export const EDIT_PRODUCT_MENU = "EDIT_PRODUCT_MENU";
export const REFRECH_PRODUCT_MENU = "REFRECH_PRODUCT_MENU";
export const REMOVE_PRODUCT_MENU = "EDIT_PRODUCT_MENU";
