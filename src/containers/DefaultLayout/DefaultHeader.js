import React, { useEffect, useState } from "react";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../../assets/img/brand/iphood.png";
import sygnet from "../../assets/img/brand/symbole.png";
import { withRouter } from "react-router-dom";
import { logout } from "../../providers/actions/auth";

const DefaultHeader = ({ history, user, logout }) => {
  const [currentuser, setcurrentuser] = useState({
    isAdmin: "",
    isSuperAdmin: "",
    createdAt: "",
    restaurant: "",
    _id: "",
    firstName: "",
    lastName: "",
    email: "",
  });
  useEffect(() => {
    if (user) setcurrentuser(user);
  }, [user]);

  return (
    <React.Fragment>
      <AppSidebarToggler className="d-lg-none" display="md" mobile />
      <AppNavbarBrand
        full={{ src: logo, width: 89, alt: "Logo" }}
        minimized={{ src: sygnet, width: 30, alt: "Logo" }}
      />

      <AppSidebarToggler className="d-md-down-none" display="lg" />

      <Nav className="ml-auto" navbar>
        {currentuser.firstName} {currentuser.lastName}
        <UncontrolledDropdown nav direction="down">
          <DropdownToggle nav>
            <img src={logo} className="img-avatar" alt="s" />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem onClick={(e) => console.log("profile clicked ")}>
              <i className="fa fa-lock"></i> Profile
            </DropdownItem>
            <DropdownItem onClick={logout}>
              <i className="fa fa-lock"></i> Se déconnecter
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Nav>
    </React.Fragment>
  );
};

DefaultHeader.prototype = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  user: state.auth.user,
});
export default connect(StateProps, { logout })(withRouter(DefaultHeader));
